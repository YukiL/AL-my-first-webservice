import web
import xml.etree.ElementTree as ET


# Loading file
# tree = ET.parse('user_data.xml')
# root = tree.getroot()

# Generating routes
urls = (
    '/users', 'list_users',
    '/c=(.*)', 'get_celcius',
    '/f=(.*)', 'get_fahrenheit',
    '/k=(.*)', 'get_kelvin',
    '/users/(.*)', 'get_user',
)
app = web.application(urls, globals())


class hello:
    def GET(self, name):
        if not name:
            name = 'World'
        return 'Hello, ' + name + '!'


class list_users:
    def GET(self):
        output = 'users: '
        users = []
        for child in root:
            users.append(child.attrib)
        output += str(users)
        return output


class get_user:
    def GET(self, user):
        for child in root:
            if child.attrib['id'] == user:
                return str(child.attrib)

class get_celcius:
    def GET(self, temp):
        C = float(temp)
        print('C', C, type(C))
        F = (C * (9 / 5)) + 32
        print('F', F)
        K = C + 273.15
        print('K', K)

        temp_lowercase = temp.lower()

        contains_letters = temp_lowercase.islower()

        if contains_letters == True:
            get_error()
        else:
            print('else')
            root = ET.Element('temperature')
            celcius = ET.SubElement(root, 'celcius')
            celcius.text = C
            fahrenheit = ET.SubElement(root, 'fahrenheit')
            fahrenheit.text = F
            kelvin = ET.SubElement(root, 'kelvin')
            kelvin.text = K

            print(root)



# class get_fahrenheit:
#     def GET(self, temp):
#         print(type(temp))
#
#         F = temp
#         C = (F-32)/1.8
#         K = (F-32) * (5/9) + 273.15
#
#         if type(temp) == 'str':
#             get_error()
#         else:
#             root = ET.Element('temperature')
#             celcius = ET.SubElement(root, 'celcius')
#             celcius.text = C
#             fahrenheit = ET.SubElement(root, 'fahrenheit')
#             fahrenheit.text = F
#             kelvin = ET.SubElement(root, 'kelvin')
#             kelvin.text = K
#
#             print(ET.tostring(root))

# class get_kelvin:
#     def GET(self, temp):
#         print(type(temp))
#         K = temp
#         C = K-273.15
#         F = (K − 273) * (9/5) + 32
#
#         if type(temp) == 'str':
#             get_error()
#         else:
#             root = ET.Element('temperature')
#             celcius = ET.SubElement(root, 'celcius')
#             celcius.text = C
#             fahrenheit = ET.SubElement(root, 'fahrenheit')
#             fahrenheit.text = F
#             kelvin = ET.SubElement(root, 'kelvin')
#             kelvin.text = K
#
#             print(ET.tostring(root))

class get_error:
    print('Value is not a number')



if __name__ == "__main__":
    app.run()
